/*******************************************************************************
 * Author: Sarah Maas
 * Date: March 11, 2018
 * Description: This program is a file transfer server written to pair with a
 * python client. It is a 2-connection client-server network application 
 * utilizing separate connections for control and data. 
/*******************************************************************************

/* 
    Program Outline:
    * ftserve starts on host A, validates the parameter, and waits on
      <SERVER_PORT> for client request
    * ftclient starts on host B, and validates parameters
    * ftserve and ftclient establish a control connection
    * ftclient sends a command
    * ftserve accepts and interprets command
    * ftserve and ftclient send/receive control messages on the control
      connection
    * ftserve and ftclient establish a data connection
    * When appropriate, ftserve sends its directory to ftclient on the data
      connection, and ftclient displays the directory on-screen
    * When appropriate, ftserve validates filename , and either sends the
      contents of filename on the data connection or sends an appropriate
      error message to ftclient on the control connection
    * ftclient saves the file, and handles "duplicate file name" error if
      necessary
    * ftclient displays "transfer complete" message on-screen
    * ftserve closes the data connection after directory or file transfer
    * Control connection closed by ftclient
    * ftserve repeats until terminated by a supervisor  
*/
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>

#define PACKET_SIZE 256
#define BUFFER_SIZE 100000001 
#define HOST_SIZE 1024
#define PORT_SIZE 20
#define PATH_SIZE 500
#define SEPARATOR "@@"
#define TERMINATOR "$$"

#define SET_BOLD_RED        "\033[1;31m"
#define SET_RED             "\033[0;31m"
#define SET_BOLD_GREEN      "\033[1;32m"
#define SET_GREEN           "\033[0;32m"
#define SET_BOLD_YELLOW     "\033[1;33m"
#define SET_YELLOW          "\033[0;33m"
#define SET_BOLD_BLUE       "\033[1;34m"
#define SET_BLUE            "\033[0;34m"
#define SET_BOLD_MAGENTA    "\033[1;35m"
#define SET_MAGENTA         "\033[0;35m"
#define SET_BOLD_CYAN       "\033[1;36m"
#define SET_CYAN            "\033[0;36m"
#define RESET_COLOR         "\033[0m"

// required functions:
// startup
// handleRequest

typedef struct {
	char* option;
	char* filename;
} Command;

typedef struct {
	char server[HOST_SIZE];
	char port[PORT_SIZE];
	char path[PATH_SIZE];
	char client[HOST_SIZE];
	char data_port[PORT_SIZE];
} Connection;

// function prototypes
void error(const char* msg);
void startup(char* port);
void ftpSession(int socket, struct sockaddr_in* clientAddress);
int connectToClient(int port, struct sockaddr_in* inAddress);
char* listDirectory();
void readFile(char** storedText, char* fileName, int* length);
void setSignalHandlers();
void catchSIGCHLD(int signo);
int initiateContact(int socket);
Command handleRequest(int socket);
char* processCommand(Command cmd, int socket, int* length);
void sendFileSize(int length, int socket);
void sendMessage(char buffer[], int socket);
void sendLongMessage(char* message, int socket, int* length);

// stores session info, all client info set after fork
Connection session;

int main(int argc, char* argv[]) {
    // validate arguments, test for 2 inputs and that the second is a number
    if (argc != 2 || atoi(argv[1]) < 0) {
        fprintf(stderr, "%s <port#>\n", argv[0]);
        exit(0);
    }
    // clear session details
    memset((char*)&session, '\0', sizeof(Connection));
    // reap zombie processes
    setSignalHandlers();
    // start server
    startup(argv[1]);
    return 0; 
}


/******************************************************************************* 
 * startup: This function starts the FTP server.
 ******************************************************************************/
void startup(char* port) {
    int listenSocketFD, establishedConnectionFD, portNumber, charsRead;
    socklen_t sizeOfClientInfo;
	//char buffer[BUFFER_SIZE];
	//memset(g_buffer, '\0', sizeof(g_buffer));
	struct sockaddr_in serverAddress, clientAddress, clientAddressOutbound;
    pid_t pid;

	// set up what we know for session Connection struct
	session.path[0] = '.';
	gethostname(session.server, sizeof(session.server) - 1);
	snprintf(session.port, sizeof(session.port) - 1, "%s", port);
	printf("%sHost: %s\nPort: %s%s\n\n", SET_BOLD_YELLOW, session.server, 
		session.port,RESET_COLOR); // TODO remove
	
    // Set up the address struct for this process (the server)
    // Clear out the address struct
	memset((char *)&serverAddress, '\0', sizeof(serverAddress)); 
	// Get the port number, convert to an integer from a string
	portNumber = atoi(port); 
	// Create a network-capable socket
	serverAddress.sin_family = AF_INET;
	// Store the port number
	serverAddress.sin_port = htons(portNumber); 
	// Any address is allowed for connection to this process
	serverAddress.sin_addr.s_addr = INADDR_ANY; 

	// Set up the socket
	// Create the socket
	listenSocketFD = socket(AF_INET, SOCK_STREAM, 0); 
	if (listenSocketFD < 0) {
		error("SERVER: ERROR opening socket");
	}

	// Enable the socket to begin listening
	if (bind(listenSocketFD, (struct sockaddr *)&serverAddress, 
		sizeof(serverAddress)) < 0) {
		// Connect socket to port
		error("SERVER: ERROR on binding");
	}
	// Flip the socket on - it can now receive up to 5 connections
	listen(listenSocketFD, 5);     
	printf("%sServer open for up to 5 simultaneous connections on port %s.%s\n", 
		SET_BOLD_GREEN, port, RESET_COLOR);
    // continue until program interrupted
	// keep parent running to accept new connections
	while(1) {
		// Accept a connection, blocking if one is not available until one connects
		// Get the size of the address for the client that will connect
		sizeOfClientInfo = sizeof(clientAddress); 
		// Accept
		establishedConnectionFD = accept(listenSocketFD, 
			(struct sockaddr *)&clientAddress, &sizeOfClientInfo); 
		if (establishedConnectionFD < 0) {
			error("SERVER: ERROR on accept");
			continue;
		}
	
		pid = fork();
		if (pid == 0) {
			// I am the child
			// Close the listening socket
			close(listenSocketFD); 
            // starts ftp session
            ftpSession(establishedConnectionFD, (struct sockaddr_in *)&clientAddress);
			// Close the existing socket which is connected to the client
			close(establishedConnectionFD); 

			printf("%sClient connection finished from %s.%s\n\n", 
				SET_BOLD_BLUE, session.client, RESET_COLOR);
			exit(0);
		} else {
			close(establishedConnectionFD);
		}
	}
}


/******************************************************************************* 
 * ftpSession: This function runs an FTP session.
 ******************************************************************************/
void ftpSession(int socket, struct sockaddr_in* clientAddress) {
	// initial control connection, receive data port
	int dataPort = initiateContact(socket);
	printf("%sNew client connection established from %s.%s\n", 
		SET_BOLD_BLUE, session.client, RESET_COLOR);
	// initiate data connection
	int dataSocket = connectToClient(dataPort, clientAddress);
    // get command
    Command cmd = handleRequest(socket);
    // process command and SEND size of result
    int length = 0;
    char* message = processCommand(cmd, socket, &length);
	if (message != NULL) {
	    // data port 
	    // send data - file contents or directory listing
	    sendLongMessage(message, dataSocket, &length);
	}
	free(cmd.option);
	free(cmd.filename);
	free(message);
	// Close the socket
	close(dataSocket);
}


/******************************************************************************* 
 * connectToClient: This function connects to the client and returns the socket.
 ******************************************************************************/
int connectToClient(int port, struct sockaddr_in* inAddress) {
	int socketFD, portNumber, charsWritten, charsRead;
	struct sockaddr_in outAddress;
	// Set up the server address struct
	// Clear out the address struct
	memset((char*)&outAddress, '\0', sizeof(outAddress)); 
	// Create a network-capable socket
	outAddress.sin_family = AF_INET;
	// Store the port number
	outAddress.sin_port = htons(port); 
	
	// Copy in the addresss
	memcpy(&outAddress.sin_addr, &inAddress->sin_addr, sizeof(inAddress->sin_addr));
	
	// Set up the socket
	// Create the socket
	socketFD = socket(AF_INET, SOCK_STREAM, 0); 
	if (socketFD < 0) {
		error("CLIENT: ERROR opening socket");
	}	
	// Connect to server
	if (connect(socketFD, (struct sockaddr*)&outAddress, 
		sizeof(outAddress)) < 0) {
		// Connect socket to address
		fprintf(stderr, "CLIENT: ERROR connecting to port %d\n", port);
		exit(2);
	}
	return socketFD;
}


/******************************************************************************* 
 * error: This function reports issues.
 ******************************************************************************/
void error(const char* msg) {
	perror(msg); 
	exit(1); 
} 


/******************************************************************************* 
 * initiateContact: This function initiates contact and gets the data port.
 ******************************************************************************/
int initiateContact(int socket) {
	// receive port number
	char buffer[PACKET_SIZE];
	char response;
	int data_port;
	memset(buffer, '\0', sizeof(buffer));
	// Read data from the socket, leaving \0 at end
	int charsRead = recv(socket, buffer, sizeof(buffer) - 1, 0); 
	if (charsRead < 0) {
		error("SERVER: ERROR reading from socket");
		response = 'F';
		data_port =  -1;
	} else {
		// success, store data port received
		response = 'S';
		data_port = atoi(buffer);
		
	}
	// send acknowledgement
	memset(buffer, '\0', sizeof(buffer));
	sprintf(buffer, "%c", response);
	int charsSent = send(socket, buffer, strlen(buffer), 0); 
	if (charsSent < 0) {
		error("SERVER: ERROR writing to socket");
	}
	snprintf(session.data_port, sizeof(session.data_port) - 1, "%d", data_port);
	
	// receive host name
	charsRead = recv(socket, session.client, sizeof(session.client) - 1, 0); 
	if (charsRead < 0) {
		error("SERVER: ERROR reading from socket");
		response = 'F';
		data_port =  -1;
	}
	printf("%sClient: %s\nPort: %s%s\n\n", SET_BOLD_YELLOW,session.client, 
		session.data_port, RESET_COLOR); // TODO remove
	// send host name
	charsSent = send(socket, session.server, sizeof(session.server) - 1, 0); 
	if (charsSent < 0) {
		error("SERVER: ERROR writing to socket");
	}
	
	return data_port;
}


/******************************************************************************* 
 * handleRequest: This function gets the command from the client.
 ******************************************************************************/
Command handleRequest(int socket) {
	Command cmd;
	cmd.option = NULL;
	cmd.filename = NULL;
	char packet[PACKET_SIZE];
	char response;
	memset(packet, '\0', sizeof(packet));
	// receive command
	// Read data from the socket, leaving \0 at end
	int charsRead = recv(socket, packet, sizeof(packet) - 1, 0); 
	if (charsRead < 0) {
		error("SERVER: ERROR reading from socket");
		response = 'F';
	} else {
		// store received value
		cmd.option = calloc(strlen(packet) + 1, sizeof(char));
		strcat(cmd.option, packet);
		response = 'S';
	}
	// send acknowledgement
	memset(packet, '\0', sizeof(packet));
	sprintf(packet, "%c", response);
	int charsSent = send(socket, packet, strlen(packet), 0); 
	if (charsSent < 0) {
		error("SERVER: ERROR writing to socket");
		// client exits, so return
		return;
	}
	// receive filename
	if (strncmp(cmd.option, "-g", 2) == 0) {
		memset(packet, '\0', sizeof(packet));
		// receive filename
		// Read data from the socket, leaving \0 at end
		int charsRead = recv(socket, packet, sizeof(packet) - 1, 0); 
		if (charsRead < 0) {
			error("SERVER: ERROR reading from socket");
		} else {
			// store received value
			cmd.filename = calloc(strlen(packet) + 1, sizeof(char));
			sscanf(packet, "%s", cmd.filename);
		}		
	}
	
	return cmd;
}


/******************************************************************************* 
 * processCommand: This function processes the command from the client and 
 * handles the control connection functionality.
 ******************************************************************************/
char* processCommand(Command cmd, int socket, int* length) {
    // process command and SEND size of result
    char* message = NULL;
    int errors = 0;
    //int length = 0;
    if (strncmp(cmd.option, "-l", 2) == 0) {
		printf("%sList directory requested on port %s.%s\n",
			SET_BOLD_MAGENTA, session.data_port, RESET_COLOR);
    	// get directory listing
    	message = listDirectory();
    	*length = strlen(message);
		if (message == NULL) {
			errors++;
			asprintf(&message, "Error reading directory");
		}    	
    } else if (strncmp(cmd.option, "-g", 2) == 0) {
		printf("%sFile \"%s\" requested on port %s.%s\n",
			SET_BOLD_CYAN, cmd.filename, session.data_port, RESET_COLOR);
		// get file contents
		readFile(&message, cmd.filename, length);
		if (message == NULL) {
			errors++;
			asprintf(&message, "FILE NOT FOUND");
			printf("%sFile \"%s\" not found. Sending error message to %s:%s.%s\n",
				SET_BOLD_RED, cmd.filename, session.client, session.port, RESET_COLOR);
		}
    } else {
		errors++;
		asprintf(&message, "INVALID COMMAND");
		printf("%sInvalid command. Sending error message to %s:%s.%s\n",
				SET_BOLD_RED, session.client, session.port, RESET_COLOR);
    }
    
    // send size of message - size 0 means error
    int size = (errors == 0) ? *length: 0;
    sendFileSize(size, socket);
    // receive acknowledgement of size from client
    char packet[2];
    memset(packet, '\0', sizeof(packet));
    recv(socket, packet, sizeof(packet) - 1, 0);
    // send error
	if (errors > 0) {
	    char packet[PACKET_SIZE];
	    memset(packet, '\0', sizeof(packet));
		strcat(packet, message);  
		sendMessage(packet, socket);
		return NULL;
	}
	return message;
}


/******************************************************************************* 
 * sendFileSize: This function sends file length through the given socket.
 ******************************************************************************/
void sendFileSize(int length, int socket) {
	char packet[PACKET_SIZE];
	memset(packet, '\0', sizeof(packet));
	sprintf(packet, "%d", length);
	// Write packets until string length is reached
	int totalCharsWritten = 0;
	while (totalCharsWritten < strlen(packet)) {
		// Write data to the socket, leaving \0 at end
		int charsWritten = send(socket, packet, sizeof(packet) - 1, 0);
		if (charsWritten < 0) {
			error("SERVER: ERROR writing to socket");
		}
		// test if all of the packet was written
		if (charsWritten < strlen(packet)) {
			fprintf(stderr, "SERVER: WARNING: Not all data written to socket!\n");
			fflush(stdout);
		} else {
			// only increment to escape loop if all written
			totalCharsWritten += charsWritten;
		}
	}	
}


/******************************************************************************* 
 * sendMessage: This function sends the given message through the given socket.
 ******************************************************************************/
void sendMessage(char buffer[], int socket) {
	char packet[PACKET_SIZE];
	// Write packets until string length is reached
	int totalCharsWritten = 0;
	while (totalCharsWritten < strlen(buffer)) {
		memset(packet, '\0', sizeof(packet));
		memcpy(packet, buffer + totalCharsWritten, sizeof(packet) - 1);
		// Write data to the socket, leaving \0 at end
		int charsWritten = send(socket, packet, sizeof(packet) - 1, 0);
		if (charsWritten < 0) {
			error("SERVER: ERROR writing to socket");
		}
		// test if all of the packet was written
		if (charsWritten < strlen(packet)) {
			fprintf(stderr, "SERVER: WARNING: Not all data written to socket!\n");
			fflush(stdout);
		}
		totalCharsWritten += charsWritten;	
	}
}


/******************************************************************************* 
 * sendLongMessage: This function sends the given message of unlimited size 
 * through the given socket.
 ******************************************************************************/
void sendLongMessage(char* message, int socket, int* length) {
	char packet[PACKET_SIZE];
	// Write packets until string length is reached
	int totalCharsWritten = 0;
	while (totalCharsWritten < *length) {
		memset(packet, '\0', sizeof(packet));
		memcpy(packet, message + totalCharsWritten, sizeof(packet) - 1);
		//printf("PACKET: '%s'\n", packet);
		// Write data to the socket, leaving \0 at end
		int charsWritten = send(socket, packet, sizeof(packet) - 1, 0);
		if (charsWritten < 0) {
			error("SERVER: ERROR writing to socket");
		}
		// test if all of the packet was written
		if (charsWritten < strlen(packet)) {
			fprintf(stderr, "SERVER: WARNING: Not all data written to socket!\n");
			fflush(stdout);
		}
		totalCharsWritten += charsWritten;	
	}
}


/******************************************************************************* 
 * listDirectory: This function stores the directory listing in memory.
 ******************************************************************************/
char* listDirectory() {
	char* listing = calloc(BUFFER_SIZE, sizeof(char));

	//DIR* currDir = opendir(".");
	//strcat(session.path, "/");
	//strcat(session.path, "iamadir");
	DIR* currDir = opendir(session.path);
	struct dirent *fileInDir; 
	struct stat dirAttributes; 

	if (currDir > 0) {
		// check each entry in directory, readdir iterates through them
		while ((fileInDir = readdir(currDir)) != NULL) {
            // if it's . or .. skip to next
            if (strstr("..", fileInDir->d_name)!=NULL) {
                continue;
            }
            // get attributes of the file 
            stat(fileInDir->d_name, &dirAttributes);
            if (S_ISREG(dirAttributes.st_mode)) {
	            // store file name
	            strcat(listing, fileInDir->d_name);
	            strcat(listing, "\n");
            } else {
            	printf("%s%s not a regular file, excluding from list.%s\n", 
            		SET_BOLD_RED, fileInDir->d_name, RESET_COLOR);
            }
		}
		printf("%sSending directory contents to %s:%s.%s\n",
			SET_BOLD_MAGENTA, session.client, session.data_port, RESET_COLOR);
	} else {
		free(listing);
		error("SERVER: opendir");
	}
	return listing;
}


/******************************************************************************* 
 * readFile: This function stores text from file in memory.
 ******************************************************************************/
void readFile(char** storedText, char* fileName, int* length) {
	//int length;
	char* tmp = (char*)storedText;
	// open file
	FILE* fp = fopen(fileName, "rb");
	if(fp == NULL){
	    //perror("SERVER ERROR: cannot find file"); // reported elsewhere
	} else {
		// get file length
		fseek(fp, 0, SEEK_END);
		*length = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		// malloc len + 1 and copy file contents into string
		*storedText = calloc(*length + 1, sizeof(char));
		fread(*storedText, *length, 1, fp);
		fclose(fp);

		printf("%sSending \"%s\" to %s:%s.%s\n", SET_BOLD_CYAN, fileName, 
			session.client, session.data_port, RESET_COLOR);
	}
}


/******************************************************************************* 
 * setSignalHandlers: This function sets up and activates signal handlers for 
 * SIGINT and SIGTSTP.
 ******************************************************************************/
void setSignalHandlers() {
    struct sigaction SIGCHLD_action = {0};
    // SIGCHLD parameter setup
    SIGCHLD_action.sa_handler = catchSIGCHLD;
    sigfillset(&SIGCHLD_action.sa_mask);
    SIGCHLD_action.sa_flags = SA_RESTART;
    // activate signal handlers
    sigaction(SIGCHLD, &SIGCHLD_action, NULL);
}


/******************************************************************************* 
 * catchSIGCHLD: This function is a signal handler for SIGCHLD. It reaps the
 * zombie processes.
 ******************************************************************************/
void catchSIGCHLD(int signo) {
	while(waitpid(-1, NULL, WNOHANG) > 0);
}


/******************************************************************************* 
 References:

 My 344 assignment for block 4 - OTP. 
    * directly used/ minor chages: sendMessage, readFile, error 
    * setSignalHandlers, catchSIGCHLD
    * refactored from main and used: startup

 My 344 assignment for block 2 - adventure.
	* influenced: listDirectory

 For structure of the connections:
 https://github.com/caperren/CS372/tree/master/Project%202
 
 Beej's guide for hostname.
 
 Reused color code from project 1 for debugging and then to color code
 messages output to console.
 http://web.theurbanpenguin.com/adding-color-to-your-output-from-c/
 *******************************************************************************/
