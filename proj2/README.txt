__________________________________________________________

RUN PROGRAM
__________________________________________________________
Server
make
./ftserve <port#>
__________________________________________________________
Client
chmod u+x ftclient
./ftclient <host> <port#> -l <recv port#>  
    or 
./ftclient <host> <port#> -g <filename> <recv port#>

-l retrieves a listing of the server directory
-g <filename> retrieves the given file if it exists
__________________________________________________________
FTP - Client/Server interaction

After starting the server on a port, it will be ready to 
accept connections from clients.

When starting an instance of the client, all input is 
given via command line from the user. This is then 
processed and returns data from the server. If the option
-l is used, a listing of the files in the directory on 
the server that are eligible for file transfer are given.
If the option -g is used, it must be followed by a valid
filename to then retrieve and save a copy of that file
locally. If the file already exists, it will notify and
save as a new file with a number appended. When finished,
it displays "Transfer complete!" onscreen.

The server will continue running until Ctrl-C is pressed
to send the SIGINT signal.
__________________________________________________________
Testing machine

I ran the server on flip2.engr.oregonstate.edu and when
testing was able to connect to it from flip1 and 
flip2 (as localhost).

Files have been tested successfully up to a size of 100MB.
__________________________________________________________
Extra credit?

Binary files are supported - nothing special required to 
do this, just supply the filename. When tested on flip, 
you should be transferring files that already exist, so 
I tested this with the compiled ftserver and tested with:
diff -s ftserver ftserver-1
Server supports up to 5 simultaneous connections.
Colored output.
__________________________________________________________
