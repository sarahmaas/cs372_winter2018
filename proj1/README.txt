__________________________________________________________

RUN PROGRAM
__________________________________________________________
Server
chmod u+x chatserve
./chatserve <port#>
__________________________________________________________
Client
make
./chatclient <server-hostname> <port#>
__________________________________________________________
Chat - Client/Server interaction

After starting server and then client, the client will
prompt for a username and truncate anything more than 
10 characters.

Enter a chat handle that is 10 characters or less: _______

The client will then prompt for message entry. Once 
entered, chat will alternate between server and client. 

At any time, the client or server can exit the chat
connection by entering '\quit'. When this happens,
the connection will close, the client will end 
execution, and the server will wait for another client.

There is a 500 character limit for messages. The server
prompts for a new message and the client just truncates.

The server will continue running until Ctrl-C is pressed
to send the SIGINT signal.

__________________________________________________________
Testing machine

I ran the server on flip3.engr.oregonstate.edu and was 
able to connect to it from flip1, flip2, and flip3.

__________________________________________________________
Extra credit?

Colored output :)

__________________________________________________________


