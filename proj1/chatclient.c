/*******************************************************************************
 * Author: Sarah Maas
 * Date: February 11, 2018
 * Description: This program is a chat client written to pair with a python 
 * server. Given the server's host and port, it prompts the user for a username
 * and then proceeds to send and receive messages.
 *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define SET_BOLD_RED        "\033[1;31m"
#define SET_RED             "\033[0;31m"
#define SET_BOLD_GREEN      "\033[1;32m"
#define SET_GREEN           "\033[0;32m"
#define SET_BOLD_YELLOW     "\033[1;33m"
#define SET_YELLOW          "\033[0;33m"
#define SET_BOLD_BLUE       "\033[1;34m"
#define SET_BLUE            "\033[0;34m"
#define SET_BOLD_MAGENTA    "\033[1;35m"
#define SET_MAGENTA         "\033[0;35m"
#define SET_BOLD_CYAN       "\033[1;36m"
#define SET_CYAN            "\033[0;36m"
#define RESET_COLOR         "\033[0m"


#define HANDLE_LENGTH 10
#define MESSAGE_LENGTH 500
#define BUFFER_SIZE (HANDLE_LENGTH + 2 + MESSAGE_LENGTH + 1)

typedef enum {
	FALSE,
	TRUE
} boolean;

// function prototypes
void error(const char* msg);
int connectToServer(char* host, char* port);
void getHandle(char handle[]);
char* getMessage(char handle[]);
boolean sendMessage(char buffer[], int bufferSize, int socket);
boolean receiveMessage(char buffer[], int bufferSize, int socket);

int main(int argc, char* argv[]) {
	int socketFD;
	char buffer[MESSAGE_LENGTH + 1] = {0};
	
	// test for 4 inputs and that the fourth is a number
	if (argc != 3 || atoi(argv[2]) < 0) {
		// prints error that shows correct syntax
		fprintf(stderr, "%s <server-hostname> <port#>\n", argv[0]);
		exit(0);
	}
	// get handle 
	char handle[HANDLE_LENGTH + 1] = {0};
	getHandle(handle);
	
	// start connection to server
	socketFD = connectToServer(argv[1], argv[2]);
	
	// run chat
	while (TRUE) {
		boolean connectStatus = TRUE;
		// get message
		char* message = getMessage(handle);
		memset(buffer, '\0', sizeof(buffer));
		// limit to 500 characters
		sprintf(buffer, "%s> %.*s", handle, MESSAGE_LENGTH, message);
		// quit program if quit message entered
		if (strncmp(message, "\\quit", 5) == 0) {
			printf("%sClosing connection%s\n", SET_RED, RESET_COLOR);
			fflush(stdout);
			break;
		}
		
		// send message
		sendMessage(buffer, sizeof(buffer), socketFD);
		
		// receive message
		connectStatus = receiveMessage(buffer, sizeof(buffer), socketFD);
		if (connectStatus == FALSE)
			break;
	}
	// Close the socket
	close(socketFD);         
	// clean up memory
	
	return 0;
}


/******************************************************************************* 
 * error: This function reports issues.
 ******************************************************************************/
void error(const char* msg) {
	perror(msg); 
	exit(0); 
} 

/******************************************************************************* 
 * connectToServer: This function connects to the server and returns the socket.
 ******************************************************************************/
int connectToServer(char* host, char* port) {
	int socketFD, portNumber, charsWritten, charsRead;
	struct sockaddr_in serverAddress;
	struct hostent* serverHostInfo;
	
	// Set up the server address struct
	// Clear out the address struct
	memset((char*)&serverAddress, '\0', sizeof(serverAddress)); 
	// Get the port number, convert to an integer from a string
	portNumber = atoi(port); 
	// Create a network-capable socket
	serverAddress.sin_family = AF_INET;
	// Store the port number
	serverAddress.sin_port = htons(portNumber); 
	// Convert the machine name into a special form of address
	serverHostInfo = gethostbyname(host); 
	if (serverHostInfo == NULL) { 
		fprintf(stderr, "CLIENT: ERROR, no such host\n"); 
		exit(0); 
	}
	// Copy in the address
	memcpy((char*)&serverAddress.sin_addr.s_addr, 
	(char*)serverHostInfo->h_addr, serverHostInfo->h_length); 
	
	// Set up the socket
	// Create the socket
	socketFD = socket(AF_INET, SOCK_STREAM, 0); 
	if (socketFD < 0) {
		error("CLIENT: ERROR opening socket");
	}	
	// Connect to server
	if (connect(socketFD, (struct sockaddr*)&serverAddress, 
		sizeof(serverAddress)) < 0) {
		// Connect socket to address
		fprintf(stderr, "CLIENT: ERROR connecting to port %s\n", port);
		exit(2);
	}
	return socketFD;
}


/******************************************************************************* 
 * getMessage: This function gets an appropriately sized handle from the user.
 ******************************************************************************/
void getHandle(char handle[]) {
	do {
		printf("Enter a chat handle that is 10 characters or less: ");
		fflush(stdout);
		scanf(" %10s", handle);
		// clear out the rest of the buffer
		while(getchar() != '\n');
	} while(strlen(handle) == 0);
}


/******************************************************************************* 
 * getMessage: This function will replace a given substring with another
 * given substring and returns the new string. 
 ******************************************************************************/
char* getMessage(char handle[]) {
	char* userInput = NULL;
	size_t len = 0;
	ssize_t read;
	// continue to get input until it doesn't fail
	while(TRUE) {
		len = 0;
		// prompt user for command, output handle to screen
		printf("%s> ", handle);
		fflush(stdout); 
		// read in the command from the user
		read = getline(&userInput, &len, stdin);
		fflush(stdin);
		// handle failure of getline
		if (read == -1) {
			clearerr(stdin);
			free(userInput);
		} else {
			break;
		} 
	}
	userInput[read - 1] = '\0';
	// return command
	return userInput;
}


/******************************************************************************* 
 * sendMessage: This function sends the given message through the given socket.
 ******************************************************************************/
boolean sendMessage(char buffer[], int bufferSize, int socket) {
	// Write data to the socket, leaving \0 at end
	int charsWritten = send(socket, buffer, bufferSize - 1, 0);
	if (charsWritten < 0) {
		error("CLIENT: ERROR writing to socket");
	}
	// test if all of the buffer was written
	if (charsWritten < strlen(buffer)) {
		fprintf(stderr, "CLIENT: WARNING: Not all data written to socket!\n");
		fflush(stdout);
	}
	
}


/******************************************************************************* 
 * receiveMessage: This function receives a message through the given socket.
 ******************************************************************************/
boolean receiveMessage(char buffer[], int bufferSize, int socket) {
	// Clear out the buffer again for reuse
	memset(buffer, '\0', bufferSize); 
	// Read data from the socket, leaving \0 at end
	int charsRead = recv(socket, buffer, bufferSize - 1, 0); 
	if (charsRead < 0) {
		error("CLIENT: ERROR reading from socket");
	}
	// output message if connection closed or what was received
	if (charsRead == 0) {
		printf("%sConnection closed%s\n", SET_RED, RESET_COLOR);
		return FALSE;
	} else {
		printf("%s%s%s\n", SET_BOLD_CYAN, buffer, RESET_COLOR);
		return TRUE;
	}
}


/******************************************************************************* 
 References:

 My 344 assignment for block 4, stripped way down.

 http://web.theurbanpenguin.com/adding-color-to-your-output-from-c/
 *******************************************************************************/